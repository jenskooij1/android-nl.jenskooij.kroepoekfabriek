package nl.jenskooij.kroepoekfabriek.api;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

import nl.jenskooij.kroepoekfabriek.AppController;

public class Api {

    Context context;
    ApiListener listener;

    private static final String TAG = "Kroepoekfabriek (Api)";

    private static final String BASE_URL = "http://mojo.jnsmvc.nl/";

    public static final int METHOD_CRAWL = 1;
    public static final int METHOD_NEWS = 2;
    public static final int METHOD_REPORT = 3;
    public static final int METHOD_REGISTER = 4;
    public static final int METHOD_FRIENDS = 5;
    public static final int METHOD_CHANGE_NAME = 6;
    public static final int METHOD_ADD_FRIEND = 7;
    public static final int METHOD_LOOKUP_FRIEND = 8;
    public static final int METHOD_GET_BEACONS = 9;
    public static final int METHOD_CONFIRM_FOUND_FRIEND = 10;

    public Api(Context context, ApiListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void Crawl() {
        new JSONAsyncTask(listener, context,METHOD_CRAWL, true, 60 * 60 * 6 * 1000).execute("crawler/kroepoekfabriek");
    }

    public void GetNews() {
        new JSONAsyncTask(listener, context, METHOD_NEWS, true, 60 * 60 * 6 * 1000).execute("rss/kroepoekfabriek");
    }

    class JSONAsyncTask extends AsyncTask<String, String, String> {

        private ProgressDialog mProgressDialog;
        private InputStream mInputStream = null;
        private Context mContext;

        private String mResult = "";
        private Boolean mLocalCache = false;
        private int mLocalCacheTime = 60 * 60 * 6 * 1000;

        private ApiListener mApiListener;
        private int mMethod;

        public JSONAsyncTask(ApiListener apiListener, Context context, int method){
            this.mContext = context;
            this.mApiListener = apiListener;
            this.mMethod = method;

            mProgressDialog = new ProgressDialog(apiListener.getActivity());
        }

        /**
         *
         * @param apiListener
         * @param context
         * @param method Api.METHOD_XXX constants
         * @param localCache
         * @param localCacheTime Time in milliseconds before the app refreshes the cache again
         */
        public JSONAsyncTask(ApiListener apiListener, Context context, int method, Boolean localCache, int localCacheTime) {
            this.mContext = context;
            this.mApiListener = apiListener;
            this.mMethod = method;
            this.mLocalCache = localCache;
            this.mLocalCacheTime = localCacheTime;

            mProgressDialog = new ProgressDialog(apiListener.getActivity());
        }

        protected void onPreExecute() {
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.show();
            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface arg0) {
                    JSONAsyncTask.this.cancel(true);
                }
            });
        }

        @Override
        protected String doInBackground(String... params) {
            String url_select = BASE_URL + params[0];
            AppController.Log(TAG, "loading url: " + url_select);
            String localFile = params[0].replace("/", "-") + ".cache";
            Boolean fetch = checkLocalCache(localFile);

            try {
                if (fetch) {
                    // HttpClient is more then less deprecated. Need to change to URLConnection
                    HttpClient httpClient = new DefaultHttpClient();

                    HttpPost httpPost = new HttpPost(url_select);
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    HttpEntity httpEntity = httpResponse.getEntity();

                    // Read mContent & Log
                    mInputStream = httpEntity.getContent();
                }
            } catch (UnsupportedEncodingException e1) {
                AppController.Log("UnsupportedEncodingException", e1.toString());
                e1.printStackTrace();
            } catch (ClientProtocolException e2) {
                AppController.Log("ClientProtocolException", e2.toString());
                e2.printStackTrace();
            } catch (IllegalStateException e3) {
                AppController.Log("IllegalStateException", e3.toString());
                e3.printStackTrace();
            } catch (IOException e4) {
                AppController.Log("IOException", e4.toString());
                e4.printStackTrace();
            }
            // Convert response to string using String Builder
            try {
                if (fetch) {
                    AppController.Log(TAG, "Fetching result from API");

                    mResult = getStringFromInputStreamReader();

                    if (mLocalCache == true) {
                        writeToCache(localFile);
                    }
                } else {
                    InputStream fis = mContext.openFileInput(localFile);
                    mResult = convertStreamToString(fis);
                }
                final JSONObject resultObject = new JSONObject(mResult);

                mProgressDialog.dismiss();

                mApiListener.runOnUiThread(new Runnable() {
                    public void run() {
                        mApiListener.handleApiResult(mMethod, resultObject);
                    }
                });

                return mResult;
            } catch (Exception e) {
                AppController.Log("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            }
            return null;
        }

        private String getStringFromInputStreamReader() {
            try {
                BufferedReader bReader = new BufferedReader(new InputStreamReader(mInputStream, "iso-8859-1"), 8);
                StringBuilder sBuilder = new StringBuilder();

                String line = null;
                while ((line = bReader.readLine()) != null) {
                    sBuilder.append(line + "\n");
                }

                mInputStream.close();
                return (String) sBuilder.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private void writeToCache(String localFile) {
            try {
                OutputStream fos = mContext.openFileOutput(localFile, Context.MODE_PRIVATE);
                fos.write(mResult.getBytes());
                fos.close();
            } catch (Exception e) {
                AppController.Log(TAG, "Could not write file: " + e.toString());
                e.printStackTrace();
            }
        }

        private Boolean checkLocalCache(String localFile) {
            File file = mContext.getFileStreamPath(localFile);
            if(file.exists() && mLocalCache == true) {
                AppController.Log(TAG, "localFile: " + localFile);
                AppController.Log(TAG, "localFile lastMod: " + file.lastModified());
                Date lastMod = new Date(file.lastModified());
                Date now = new Date();
                Long age = now.getTime() - lastMod.getTime();
                Long ageInSeconds = age / 1000;
                if (age > mLocalCacheTime && isNetworkAvailable()) {
                    AppController.Log(TAG, "localCache expired. Refetch result from api. ");
                    return true;
                } else if (age > mLocalCacheTime && isNetworkAvailable() == false) {
                    AppController.Log(TAG, "localCache expired. But no internet connection, so using cache.");
                    return false;
                } else {
                    AppController.Log(TAG, "localCache is up-to-mDate and will be used.");
                    return false;
                }
            } else {
                AppController.Log(TAG, "localCache wont be used. will fetch.");
                return true;
            }
        }

        protected String convertStreamToString(InputStream is) throws Exception {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            reader.close();
            return sb.toString();
        }

        private boolean isNetworkAvailable() {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        @Override
        protected void onPostExecute(String result) {}
    }
}
