package nl.jenskooij.kroepoekfabriek.api;

import android.app.Activity;
import android.app.Fragment;

import org.json.JSONObject;

public interface ApiListener {
    public void handleApiResult(int apiMethod, JSONObject result);
    void runOnUiThread(Runnable runnable);
    public Activity getActivity();
}
