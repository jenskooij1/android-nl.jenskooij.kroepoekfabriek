package nl.jenskooij.kroepoekfabriek.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import nl.jenskooij.kroepoekfabriek.AppController;
import nl.jenskooij.kroepoekfabriek.R;

public class NewsItemActivity extends ActionBarActivity {

    private String mTitle;
    private String mDate;
    private String mDesc;
    private String mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_item);

        ((AppController) getApplication()).trackScreen("News Item Activity");

        Intent i = getIntent();
        mTitle = i.getStringExtra("title");
        mDate = i.getStringExtra("date_kf");
        mDesc = i.getStringExtra("desc");
        mContent = i.getStringExtra("content");

        setTitle(mTitle);

        setStyling();
        setValues();
    }

    private void setValues() {
        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = vi.inflate(R.layout.fragment_news_item, null);
        TextView titleView= (TextView) v.findViewById(R.id.fragment_news_title);
        titleView.setText(mTitle);

        TextView dateView = (TextView) v.findViewById(R.id.fragment_news_date_kf);
        dateView.setText(mDate);

        TextView descView = (TextView) v.findViewById(R.id.fragment_news_desc);
        if (!mDesc.isEmpty()) {
            descView.setText(mDesc);
        } else {
            descView.setText("");
        }

        TextView contentView = (TextView)findViewById(R.id.activity_news_item_content);
        contentView.setText(mContent);

        LinearLayout ll = (LinearLayout)findViewById(R.id.activity_news_item_container);
        ll.addView(v);
    }

    private void setStyling() {
        BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.drawable.bg_pattern);
        bg.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

        ScrollView sv = (ScrollView)findViewById(R.id.activity_event_scrollable);
        if (Build.VERSION.SDK_INT > 15) {
            sv.setBackground(bg);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
