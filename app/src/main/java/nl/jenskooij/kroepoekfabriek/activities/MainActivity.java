package nl.jenskooij.kroepoekfabriek.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import java.util.ArrayList;

import nl.jenskooij.kroepoekfabriek.AppController;
import nl.jenskooij.kroepoekfabriek.R;
import nl.jenskooij.kroepoekfabriek.fragments.AboutFragment;
import nl.jenskooij.kroepoekfabriek.fragments.ContactFragment;
import nl.jenskooij.kroepoekfabriek.fragments.HomeFragment;
import nl.jenskooij.kroepoekfabriek.fragments.NewsFragment;
import nl.jenskooij.kroepoekfabriek.models.NavigationDrawerItem;
import nl.jenskooij.kroepoekfabriek.adapters.NavigationDrawerListAdapter;


public class MainActivity extends ActionBarActivity {
    private static final String TAG = "Kroepoekfabriek (MainActivity)";

    protected Context mContext;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;

    private CharSequence mTitle;

    private String[] mNavMenuTitles;
    private TypedArray mNavMenuIcons;

    private ArrayList<NavigationDrawerItem> mNavDrawerItems;
    private NavigationDrawerListAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((AppController) getApplication()).trackScreen("Main Activity");

        mContext = getApplicationContext();

        setStyling();
        setNavigationDrawer(savedInstanceState);
    }

    private void setNavigationDrawer(Bundle savedInstanceState) {
        mNavMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        mNavMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        mNavDrawerItems = new ArrayList<NavigationDrawerItem>();

        // Add drawer items to array
        for (int i = 0; i < mNavMenuTitles.length; i += 1) {
            mNavDrawerItems.add(new NavigationDrawerItem(mNavMenuTitles[i], mNavMenuIcons.getResourceId(i, 0)));
        }

        mNavMenuIcons.recycle();

        mAdapter = new NavigationDrawerListAdapter(getApplicationContext(), mNavDrawerItems);
        mDrawerList.setAdapter(mAdapter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_logo,
                R.string.app_name,
                R.string.app_name){
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            displayView(0);
        }

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
    }

    private void setStyling() {
        FrameLayout container = (FrameLayout)findViewById(R.id.frame_container);
        BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.drawable.bg_pattern);
        bg.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        if (Build.VERSION.SDK_INT > 15) {
            container.setBackground(bg);
        }

        mTitle = mDrawerTitle = getTitle();
    }

    /*
    * Slide menu item click listener
    * */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    private void displayView(int position) {
        Fragment fragment = null;
        switch(position) {
            case 0:
                fragment = new HomeFragment();
                break;
            case 1:
                fragment = new NewsFragment();
                break;
            case 2:
                fragment = new AboutFragment();
                break;
            case 3:
                fragment = new ContactFragment();
                break;
            case 4:
                openLocationOnMaps();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();

            // update selected item and mTitle, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(mNavMenuTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            AppController.Log(TAG, "Error in creating fragment");
        }
    }

    private void openLocationOnMaps() {
        double latitude = 51.90231;
        double longitude = 4.348856;
        String label = "De Kroepoekfabriek";
        String uriBegin = "geo:" + latitude + "," + longitude;
        String query = "Koningin+Wilhelminahaven+ZZ+2a+3134+KG+Vlaardingen (" + label + ")";
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/mTitle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        //menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTitle(CharSequence title) {
        if (title.equals("Home")) {
            title = "Kroepoekfabriek";
        }
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public MainActivity getThis()
    {
        return MainActivity.this;
    }

}
