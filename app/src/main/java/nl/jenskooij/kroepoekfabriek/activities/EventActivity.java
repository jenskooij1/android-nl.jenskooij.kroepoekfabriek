package nl.jenskooij.kroepoekfabriek.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.io.UnsupportedEncodingException;

import nl.jenskooij.kroepoekfabriek.AppController;
import nl.jenskooij.kroepoekfabriek.R;
import nl.jenskooij.kroepoekfabriek.helpers.DownloadImageTask;


public class EventActivity extends ActionBarActivity {

    private static final String TAG = "Kroepoekfabriek (EventActivity)";

    protected String mTitle;
    protected String mDate;
    protected String mImage;
    protected String mContent;
    protected String mLink;
    private String mDoorOpen;
    private String mFromTo;
    private String mPresale;
    private String mDoor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        Intent i = getIntent();
        mTitle = i.getStringExtra("title");
        mDate = i.getStringExtra("date");
        mImage = i.getStringExtra("image");
        mContent = i.getStringExtra("content");
        mLink = i.getStringExtra("link");
        mDoorOpen = i.getStringExtra("deurOpen");
        mFromTo = i.getStringExtra("vanTot");
        mPresale = i.getStringExtra("vvk");
        mDoor = i.getStringExtra("deur");

        setTitle(mTitle);

        setStyling();
        setValues();
    }

    private void setValues() {
        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = vi.inflate(R.layout.fragment_home_event, null);

        ((AppController) getApplication()).trackScreen("Event Activity");

        TextView titleView = (TextView)v.findViewById(R.id.fragment_home_event_title);
        titleView.setVisibility(View.GONE);

        ImageView imageView = (ImageView)v.findViewById(R.id.fragment_home_event_image);
        imageView.setTag(mImage);

        TextView dateView = (TextView)v.findViewById(R.id.fragment_home_event_date);
        dateView.setText(mDate);

        new DownloadImageTask().execute(imageView);
        imageView.setPadding(50,0,50,50);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        LinearLayout ll = (LinearLayout)findViewById(R.id.activity_event_container);
        ll.addView(v);

        TextView contentView = (TextView)findViewById(R.id.activity_event_content);

        TextView buyTicketsView = (TextView)findViewById(R.id.activity_event_buy_tickets);
        if (mLink.equals("null")) {
            buyTicketsView.setVisibility(View.GONE);
        } else {
            buyTicketsView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), TicketsActivity.class);
                    i.putExtra("mTitle", mTitle);
                    i.putExtra("date", mDate);
                    i.putExtra("image", mImage);
                    i.putExtra("content", mContent);
                    i.putExtra("link", mLink);
                    i.putExtra("deurOpen", mDoorOpen);
                    i.putExtra("vanTot", mFromTo);
                    i.putExtra("vvk", mPresale);
                    i.putExtra("deur", mDoor);
                    startActivity(i);
                    finish();
                    Tracker t = ((AppController) getApplication()).getTracker(
                            AppController.TrackerName.APP_TRACKER);
                    // Build and send an Event.
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("Click on Buy Tickets")
                            .setAction("Click on Buy Tickets")
                            .setLabel(mTitle)
                            .build());
                }
            });
        }

        TextView deurOpenView = (TextView) findViewById(R.id.activity_event_deur_open);
        deurOpenView.setText(mDoorOpen);
        TextView vanTotView = (TextView) findViewById(R.id.activity_event_van_tot);
        vanTotView.setText(mFromTo);
        TextView vvkView = (TextView) findViewById(R.id.activity_event_vvk);
        vvkView.setText(mPresale);
        TextView deurView = (TextView) findViewById(R.id.activity_event_deur);
        deurView.setText(mDoor);

        contentView.setText(mContent);
    }

    private void setStyling() {

        BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.drawable.bg_pattern);
        bg.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

        ScrollView sv = (ScrollView)findViewById(R.id.activity_event_scrollable);
        if (Build.VERSION.SDK_INT > 15) {
            sv.setBackground(bg);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        } else {
            AppController.Log(TAG, "unknown id: " + id);
        }

        return super.onOptionsItemSelected(item);
    }
}
