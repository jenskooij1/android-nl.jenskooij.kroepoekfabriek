package nl.jenskooij.kroepoekfabriek.activities;

import android.content.Intent;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import nl.jenskooij.kroepoekfabriek.AppController;
import nl.jenskooij.kroepoekfabriek.R;

public class TicketsActivity extends ActionBarActivity {

    private static final String TAG = "Kroepoekfabriek (TicketsActivity)";
    protected String mTitle;
    protected String mDate;
    protected String mImage;
    protected String mContent;
    protected String mLink;

    protected WebView mWebview;
    private String mDoorOpen;
    private String mFromTo;
    private String mPresale;
    private String mDoor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tickets);

        ((AppController) getApplication()).trackScreen("Tickets Activity");

        getIntentExtras();
        setTitle(mTitle);
        handleWebview();
        setStyling();
    }

    private void handleWebview() {
        mWebview = (WebView)findViewById(R.id.activity_tickets_webview);

        mWebview.setPadding(0, 0, 0, 0);
        mWebview.setInitialScale(50);
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.getSettings().setLoadWithOverviewMode(true);
        mWebview.getSettings().setUseWideViewPort(true);
        mWebview.getSettings().setBuiltInZoomControls(false);
        mWebview.getSettings().setDefaultFontSize(28);
        mWebview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mWebview.setScrollbarFadingEnabled(false);
        mWebview.loadUrl(mLink);
        mWebview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.toLowerCase().contains("/web/orders/")) {
                    Tracker t = ((AppController) getApplication()).getTracker(
                            AppController.TrackerName.APP_TRACKER);
                    // Build and send an Event.
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("Ticket Bought")
                            .setAction("Ticket bought for " + mTitle + ": " + mPresale)
                            .setLabel(mTitle)
                            .build());
                }
                view.loadUrl(url);
                return true;
            }
        });
    }

    private void getIntentExtras() {
        Intent i = getIntent();
        mTitle = i.getStringExtra("mTitle");
        mDate = i.getStringExtra("date");
        mImage = i.getStringExtra("image");
        mContent = i.getStringExtra("content");
        mLink = i.getStringExtra("link");
        mDoorOpen = i.getStringExtra("deurOpen");
        mFromTo = i.getStringExtra("vanTot");
        mPresale = i.getStringExtra("vvk");
        mDoor = i.getStringExtra("deur");
    }

    private void setStyling() {
        BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.drawable.bg_pattern);
        bg.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

        RelativeLayout rl = (RelativeLayout)findViewById(R.id.activity_tickets_relativelayout);
        if (Build.VERSION.SDK_INT > 15) {
            rl.setBackground(bg);
            mWebview.setBackground(bg);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            Intent i = new Intent(getApplicationContext(), EventActivity.class);
            i.putExtra("mTitle", mTitle);
            i.putExtra("date", mDate);
            i.putExtra("image", mImage);
            i.putExtra("content", mContent);
            i.putExtra("link", mLink);
            i.putExtra("deurOpen", mDoorOpen);
            i.putExtra("vanTot", mFromTo);
            i.putExtra("vvk", mPresale);
            i.putExtra("deur", mDoor);
            startActivity(i);
            finish();
        } else {
            AppController.Log("Kroepoekfabriek (TicketsActivity)", "option with id: " + id);
        }

        return super.onOptionsItemSelected(item);
    }
}
