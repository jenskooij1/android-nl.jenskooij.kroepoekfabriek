package nl.jenskooij.kroepoekfabriek.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import nl.jenskooij.kroepoekfabriek.AppController;

/**
 * Created by jensk_000 on 13-1-2015.
 */
public class DownloadImageTask extends AsyncTask<ImageView, Void, Bitmap> {

    private static final String TAG = "Kroepoekfabriek (DownloadImagesTask)";

    ImageView mImageView = null;
    String mFileName;

    @Override
    protected Bitmap doInBackground(ImageView... imageViews) {
        this.mImageView = imageViews[0];
        String fileUrl = (String) mImageView.getTag();
        String[] fileUrlParts = fileUrl.split("/");
        mFileName = fileUrlParts[fileUrlParts.length - 1];

        // Check if the mImage is stored internally
        if (imageFileExists(mFileName)) {
            return getLocalImage();
        } else {
            return downloadAndStoreImage((String) mImageView.getTag());
        }
    }

    /*
    * Returns the bitmap as loaded from local storage
    * */
    private Bitmap getLocalImage() {
        Bitmap b = null;
        InputStream is;
        try {
            is = mImageView.getContext().openFileInput(mFileName);
            b = BitmapFactory.decodeStream(is);
            return b;
        } catch (FileNotFoundException e) {
            AppController.Log(TAG, "(DownloadImagesTask) Could not open file from internal storage: " + e.toString());
            e.printStackTrace();
        }
        return b;
    }

    protected Boolean imageFileExists(String fileName) {
        AppController.Log(TAG, "(DownloadImagesTask) Check if file exists: " + fileName);
        File file = mImageView.getContext().getFileStreamPath(fileName);
        return file.exists();
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        mImageView.setImageBitmap(result);
    }


    private Bitmap downloadAndStoreImage(String url) {
        Bitmap b = null;
        // Download the file
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            b = BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            AppController.Log(TAG, "(DownloadImagesTask) Could not download image: " + e.toString());
            e.printStackTrace();
        }
        // Save the file to internal storage
        try {
            mImageView.getContext();
            FileOutputStream fos = mImageView.getContext().openFileOutput(mFileName, Context.MODE_PRIVATE);
            // Writing the bitmap to the output stream
            if (b != null) {
                b.compress(Bitmap.CompressFormat.PNG, 100, fos);
            } else {
                AppController.Log(TAG, "Bitmap loaded from internetal storage is null");
            }
            fos.close();
        } catch (FileNotFoundException e) {
            AppController.Log(TAG, "Could not save image to internal storage: " + e.toString());
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            AppController.Log(TAG, "Could not save image to internal storage: " + e.toString());
        }
        return b;
    }
}
