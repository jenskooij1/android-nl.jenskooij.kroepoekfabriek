package nl.jenskooij.kroepoekfabriek.helpers;

import android.content.Context;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by jensk_000 on 27-1-2015.
 */
public class LocalCacheFileReader {

    protected String mLocalFile;
    protected Context mContext;

    public LocalCacheFileReader(String fileName, Context context) {
        this.mLocalFile = fileName + ".cache";
        this.mContext = context;
    }

    public JSONObject getContents() {
        InputStream fis = null;
        String result = null;
        JSONObject resultObject = null;
        try {
            fis = mContext.openFileInput(mLocalFile);
            result = convertStreamToString(fis);
            resultObject = new JSONObject(result);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultObject;
    }

    protected String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }


}
