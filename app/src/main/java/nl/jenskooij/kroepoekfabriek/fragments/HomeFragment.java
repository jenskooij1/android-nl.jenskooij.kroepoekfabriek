package nl.jenskooij.kroepoekfabriek.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import nl.jenskooij.kroepoekfabriek.AppController;
import nl.jenskooij.kroepoekfabriek.activities.EventActivity;
import nl.jenskooij.kroepoekfabriek.R;
import nl.jenskooij.kroepoekfabriek.api.Api;
import nl.jenskooij.kroepoekfabriek.api.ApiListener;
import nl.jenskooij.kroepoekfabriek.helpers.DownloadImageTask;

public class HomeFragment extends Fragment implements ApiListener {

    private final static String TAG = "Kroepoekfabriek (HomeFragment)";

    protected LinearLayout mEventLayout;

    public HomeFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home,
                container,
                false
        );

        ((AppController) getActivity().getApplication()).trackScreen("Home Fragment");

        Api api = new Api(getActivity().getApplicationContext(), this);
        api.Crawl();

        return rootView;
    }

    @Override
    public void handleApiResult(int apiMethod, JSONObject result) {
        if (apiMethod == Api.METHOD_CRAWL) {
            apiMethodCrawl(result);
        }
    }

    protected void apiMethodCrawl(JSONObject result) {
        try {
            JSONObject events = result.getJSONObject("result");
            JSONArray this_month_events = events.getJSONArray("this_month");
            JSONArray next_month_events = events.getJSONArray("next_month");

            addEventsToContainer(this_month_events, R.id.fragment_home_this_month_events);
            addEventsToContainer(next_month_events, R.id.fragment_home_next_month_events);
        } catch (JSONException e) {
            AppController.Log(TAG, "json error: " + e.toString());
        }
    }

    protected void addEventsToContainer(JSONArray events, int containerId) throws JSONException {
        for (int i = 0; i < events.length(); i += 1) {
            JSONObject event = (JSONObject) events.get(i);
            final String title = event.getString("title");
            final String date = event.getString("datum");
            final String image = event.getString("imageCache");
            final String content = event.getString("content");
            final String link = event.getString("link");
            final String doorOpen = event.getString("deurOpen");
            final String fromTo = event.getString("vanTot");
            final String presale = event.getString("vvk");
            final String door = event.getString("deur");

            final int finalContainerId = containerId;

            AppController.Log(TAG, "mTitle: " + title);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LayoutInflater vi = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View v = vi.inflate(R.layout.fragment_home_event, null);

                    String decodedTitle = Html.fromHtml(title).toString();
                    TextView titleView = (TextView)v.findViewById(R.id.fragment_home_event_title);
                    titleView.setShadowLayer((float) 0.02, 3, 3, Color.parseColor("#333333"));
                    titleView.setText(decodedTitle);

                    ImageView imageView = (ImageView)v.findViewById(R.id.fragment_home_event_image);
                    imageView.setTag(image);

                    TextView dateView = (TextView)v.findViewById(R.id.fragment_home_event_date);
                    dateView.setText(date);

                    new DownloadImageTask().execute(imageView);
                    imageView.setPadding(50,0,50,50);
                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(getActivity().getApplicationContext(), EventActivity.class);
                            i.putExtra("title", title);
                            i.putExtra("date", date);
                            i.putExtra("image", image);
                            i.putExtra("content", content);
                            i.putExtra("link", link);
                            i.putExtra("deurOpen", doorOpen);
                            i.putExtra("vanTot", fromTo);
                            i.putExtra("vvk", presale);
                            i.putExtra("deur", door);
                            startActivity(i);
                            // Get tracker.
                            Tracker t = ((AppController) getActivity().getApplication()).getTracker(
                                    AppController.TrackerName.APP_TRACKER);
                            // Build and send an Event.
                            t.send(new HitBuilders.EventBuilder()
                                    .setCategory("Click on Event")
                                    .setAction("Click on Event")
                                    .setLabel(title)
                                    .build());
                        }
                    });

                    mEventLayout = (LinearLayout) getView().findViewById(finalContainerId);
                    mEventLayout.addView(v);
                }
            });

        }
    }



    @Override
    public void runOnUiThread(Runnable runnable) {
        AppController.Log(TAG, "(DownloadImagesTask) runOnUiThread");
        getActivity().runOnUiThread(runnable);
    }
}
