package nl.jenskooij.kroepoekfabriek.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import nl.jenskooij.kroepoekfabriek.AppController;
import nl.jenskooij.kroepoekfabriek.R;
import nl.jenskooij.kroepoekfabriek.activities.NewsItemActivity;
import nl.jenskooij.kroepoekfabriek.api.Api;
import nl.jenskooij.kroepoekfabriek.api.ApiListener;

public class NewsFragment extends Fragment implements ApiListener {


    private final static String TAG = "Kroepoekfabriek (NewsFragment)";

    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_news,
                container,
                false
        );

        ((AppController) getActivity().getApplication()).trackScreen("News Fragment");

        Api api = new Api(getActivity().getApplicationContext(), this);
        api.GetNews();

        return rootView;
    }


    @Override
    public void handleApiResult(int apiMethod, JSONObject result) {
        if (apiMethod == Api.METHOD_NEWS) {
            apiMethodNews(result);
        } else {
            AppController.Log(TAG, "unHandled apiMethod: " + apiMethod);
        }
    }

    private void apiMethodNews(JSONObject result) {
        try {
            JSONArray news = result.getJSONArray("result");
            AppController.Log(TAG, "news: " + news);
            for (int i = 0; i < news.length(); i += 1) {
                final JSONObject newsItem = (JSONObject) news.get(i);
                AppController.Log(TAG, "mTitle: " + newsItem.getString("title"));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LayoutInflater vi = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View v = vi.inflate(R.layout.fragment_news_item, null);

                        try {
                            TextView titleView= (TextView) v.findViewById(R.id.fragment_news_title);
                            titleView.setText(newsItem.getString("title"));

                            TextView dateView = (TextView) v.findViewById(R.id.fragment_news_date_kf);
                            dateView.setText(newsItem.getString("date_kf"));

                            TextView descView = (TextView) v.findViewById(R.id.fragment_news_desc);
                            if (!newsItem.getString("desc").equals("null")) {
                                descView.setText(newsItem.getString("desc"));
                            } else {
                                descView.setText("");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            AppController.Log(TAG, "Couldnt find result in JSON: " + e.toString());
                        }

                        v.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    Intent i = new Intent(getActivity().getApplicationContext(), NewsItemActivity.class);
                                    i.putExtra("title", newsItem.getString("title"));
                                    i.putExtra("date_kf", newsItem.getString("date_kf"));
                                    i.putExtra("desc", newsItem.getString("desc"));
                                    i.putExtra("content", newsItem.getString("content"));
                                    startActivity(i);
                                    Tracker t = ((AppController) getActivity().getApplication()).getTracker(
                                            AppController.TrackerName.APP_TRACKER);
                                    // Build and send an Event.
                                    t.send(new HitBuilders.EventBuilder()
                                            .setCategory("Click on News Item")
                                            .setAction("Click on News Item")
                                            .setLabel(newsItem.getString("title"))
                                            .build());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        LinearLayout eventLayout = (LinearLayout) getView().findViewById(R.id.fragment_news_items);
                        eventLayout.addView(v);
                    }
                });




            }
        } catch (JSONException e) {
            AppController.Log(TAG, "Couldnt find result in JSON: " + e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public void runOnUiThread(Runnable runnable) {
        getActivity().runOnUiThread(runnable);
    }
}
