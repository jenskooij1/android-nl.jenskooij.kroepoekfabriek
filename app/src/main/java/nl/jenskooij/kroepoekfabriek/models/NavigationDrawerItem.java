package nl.jenskooij.kroepoekfabriek.models;

/**
 * Created by jensk_000 on 9-1-2015.
 */
public class NavigationDrawerItem {
    private String mTitle;
    private int mIcon;

    public NavigationDrawerItem(String title, int icon) {
        this.mTitle = title;
        this.mIcon = icon;
    }

    public String getTitle(){
        return this.mTitle;
    }

    public int getIcon(){
        return this.mIcon;
    }

    public void setTitle(String title){
        this.mTitle = title;
    }

    public void setIcon(int icon){
        this.mIcon = icon;
    }
}
